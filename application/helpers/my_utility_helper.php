<?php 
defined('BASEPATH') OR exit('No direct script access allowed');

// TEST : PRINT ARRAY
if (!function_exists('show_arr')) {
    function show_arr($arr)
    {
        echo "<pre>";
        print_r($arr);
        echo "</pre>";
        exit();
    }
}

/*
 * get Message from Language file
 */
if (!function_exists('lang')) {
    function lang($key, $params = array())
    {
        $CI = &get_instance();
        if ($CI->lang->line($key) === false) {
            return $key;
        }
        $line = $CI->lang->line($key);
        if (count($params) > 0) {
            return vsprintf($line, $params);
        }
        return $line;
    }
}

// TEST : PRINT LAST EXECUTED QUERY
if (!function_exists('last_query')) {
    function last_query()
    {
        $CI = &get_instance();
        return $CI->db->last_query();

    }
}
