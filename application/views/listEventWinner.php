
<?php
include_once "include/header.php";
include_once "include/sidebar.php";
?>

<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark text-bold">List Event Winner</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>

          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Table With Full Features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="eventwinnerlist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><?php echo lang("SR_NO"); ?></th>
                  <th><?php echo lang("USER_NAME") ?></th>
                  <th><?php echo  lang("EVENT_NAME"); ?></th>
                  <th><?php echo lang("WINNER_PRIZE"); ?></th>
                  <th><?php echo lang("RANK"); ?></th>
                  <th><?php echo lang("Action");  ?></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <div class="vieweventwinner"></div>

 
<?php
include_once "include/footer.php";
?>
<script type="text/javascript">

$(document).ready(function(){

    $('#eventwinnerlist').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "columns": [{
                    "orderable": false
                },
                null,
                null,
                null,
                null,
                {
                    "orderable": false
                }
            ],
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax":{
        url: "<?php echo base_url("EventWinner/listEventWinner") ?>",
        type: 'POST'
      }
    });


    //View Event
//     $(document).on("click", "#view", function(e) {
//         e.preventDefault();
//           var id = $(this).attr('data-id');
//           var url = $(this).attr("target");
//           var data_div = $(this).attr("data-div-target");
         
//           $.ajax({
//             url: url,
//             type: 'POST',
//             data:{id : id},
//             success:function(msg){    
//                 console.log(msg);
                            
//               $(".vieweventwinner").html(msg);
//               $("#view-eventwinner").modal({
//                 backdrop:'static',
//                 keyboard:true
//               });
//               $('#view-eventwinner').modal('show');
//             }
//           });
//         });


    //Delete Event Record
    $(document).on("click", "#delete", function() {
          var id = $(this).attr('data-id');          
          $.ajax({
            url: "<?php echo base_url("EventWinner/deleteEventWinner") ?>",
            type: 'POST',
            data:{id : id},
            success:function(data){
              window.location.reload();
              toastr.success('Deleted');
            }

          });
        });
});   

</script>
