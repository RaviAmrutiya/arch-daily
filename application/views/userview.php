<div class="modal fade" id="view-modal">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo lang('USER_DETAIL'); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('NAME'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['name']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('USER_NAME'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['user_name']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('MOBILE'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['mobile']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('EMAIL'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['email']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('GENDER'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['gender']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('WEB_URL'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['web_url']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('COMPANY_NAME'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['company_name']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('PROFESSION'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['profession']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('ADDRESS'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['address']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('CITY'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['city']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('STATE'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['state']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('COUNTRY'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['country']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('PIN_CODE'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['pin_code']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('STATUS'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $userdetail['status']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('CREATED_DATE'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo date("d-m-Y",strtotime($userdetail['created_date']));; ?></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>