<?php
include_once "include/header.php";
include_once "include/sidebar.php";
?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark text-bold">Add Event</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="<?php echo BASE_URL ?>Dashboard">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>

          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <div class="card card-primary">
              <div class="card-header">
                <h3 class="card-title">Add Events</h3>
              </div>
  </div>
  <form action="<?php echo BASE_URL?>Event/insertEvent" method="POST" enctype="multipart/form-data" role="form">
                <div class="card-body">
                  <div class="form-group">
                    <label for="exampleInputEmail1">Event Name</label>
                    <input type="text" class="form-control" name="eventname" id="Eventname" placeholder="Enter Event Name" required>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputFile">File input</label>
                    <div class="input-group">
                      <div class="custom-file">
                        <input type="file" name="mainbanner" class="custom-file-input" accept="image/*" id="fileinput" required>
                        <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                      </div>
                      <!-- <div class="input-group-append">
                        <span class="input-group-text" id="">Upload</span>
                      </div> -->
                    </div>
                  </div>
                  <div class="form-group">
                    <label for="exampleInputEmail1">Discription</label>
                    <textarea type="textarea" class="form-control" name="discription" id="discription" placeholder="Enter Discription" required ></textarea>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Start Date</label>
                            <input type="date" class="form-control" name="startdate" id="startdate" placeholder="Enter Start Date" required>
                      </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">End Date</label>
                            <input type="date" class="form-control" name="enddate" id="enddate" placeholder="Enter End Date" required>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Fees</label>
                            <input type="text" class="form-control" name="fees" id="fees" placeholder="Enter Fees" required>
                      </div>
                      </div>
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Max Contestant</label>
                            <input type="text" class="form-control" name="maxContestant" id="maxContestant" placeholder="Enter Max Contestant" required>
                          </div>
                      </div>
                  </div>
                  <div class="row">
                      <div class="col-md-6">
                          <div class="form-group">
                            <label for="exampleInputEmail1">Winning Prize</label>
                            <input type="text" class="form-control" name="winningprize" id="winningprize" placeholder="Enter Winning Prize" required>
                      </div>
                      </div>
                      <div class="col-md-6">
                      <div class="form-group">
                    <label>Status</label>
                    <select class="form-control" name="status">
                      <option value="open"> Open</option>
                      <option value="Close">Close</option>
                      <option value="Preparing">Preparing</option>
                    </select>
                  </div>
                      </div>
                  </div>

                  <div class="card-footer">
                  <center>
                  <button type="submit" class="btn btn-primary">Submit</button>
                </center>
                </div>
                </div>
  </form>
<?php
include_once "include/footer.php";
?>