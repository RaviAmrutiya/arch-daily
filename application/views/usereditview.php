<?php
// print_r($userdetail);exit();
include_once "include/header.php";
include_once "include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark text-bold"><?php echo lang('ADD_USER'); ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a
                                href="<?php echo BASE_URL ?>Dashboard"><?php echo lang('DASHBOARD'); ?></a></li>
                        <li class="breadcrumb-item"><?php echo lang('USER'); ?></li>
                        <li class="breadcrumb-item"><?php echo lang('ADD_USER'); ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add User</h3>
        </div>
    </div> -->
    <form action="<?php echo BASE_URL ?>User/editUser" method="POST" enctype="multipart/form-data" role="form">
    <input type="hidden" name="user_id" value="<?php echo $userdetail['user_id'] ?>">
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><?php echo lang('NAME'); ?></label>
                        <input type="text" class="form-control" name="name" id="name" value="<?php echo $userdetail['name']; ?>" placeholder="Enter your Name"
                            required>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="form-group">
                        <label for="exampleInputEmail1"><?php echo lang('USER_NAME'); ?></label>
                        <input type="text" class="form-control" name="username" id="username" value="<?php echo $userdetail['user_name']; ?>"
                            placeholder="Enter User Name" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('MOBILE'); ?></label>
                        <input type="number" class="form-control" name="mobile" id="mobile" value="<?php echo $userdetail['mobile']; ?>"
                            placeholder="Enter Mobile Number" required>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('EMAIL'); ?></label>
                        <input type="email" class="form-control" name="email" id="email" value="<?php echo $userdetail['email']; ?>"
                            placeholder="Enter Mail Address" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang("PASSWORD"); ?></label>
                        <input type="text" class="form-control" name="password" id="password" value="Cannot Edit Password"
                            placeholder="Enter Password" readonly required>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputFile"><?php echo lang('PROFILE_PIC'); ?></label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="profile_pic" class="custom-file-input" id="profile_pic"
                                    >
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class='row'>
                    <div class='col-md-6'>
                        <label>Gender</label>
                        <select class="form-control" name="gender" id="gender">
                            <option value="">---- Select Option ----</option>
                            <option value="male" <?php if($userdetail['gender'] == 'male'){echo 'selected' ;} ?>>Male</option>
                            <option value="female" <?php if($userdetail['gender'] == 'female'){echo 'selected' ;} ?>>Female</option>
                        </select>
                    </div>
                    <div class='col-md-6'>
                        <label for="exampleInputEmail1"><?php echo lang('WEB_URL'); ?></label>
                        <input type="text" class="form-control" name="web_url" id="web_url" value="<?php echo $userdetail['web_url']; ?>"  placeholder="Enter Web URL"
                            required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('COMPANY_NAME'); ?></label>
                        <input type="text" class="form-control" name="company_name" id="company_name" value="<?php echo $userdetail['company_name']; ?>"
                            placeholder="Enter Company Name" required>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('PROFESSION'); ?></label>
                        <input type="text" class="form-control" name="profession" id="profession" value="<?php echo $userdetail['profession']; ?>"
                            placeholder="Enter Profession" required>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('ADDRESS'); ?></label>
                        <textarea type="textarea" class="form-control" name="address" id="address" 
                            placeholder="Enter Address" required>
                            <?php echo $userdetail['address']; ?>
                            </textarea>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('CITY'); ?></label>
                        <input type="text" class="form-control" name="city" id="city" value="<?php echo $userdetail['city']; ?>" placeholder="Enter City" required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('STATE'); ?></label>
                        <input type="text" class="form-control" name="state" id="state" value="<?php echo $userdetail['state']; ?>" placeholder="Enter State"
                            required>
                    </div>
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('COUNTRY'); ?></label>
                        <input type="text" class="form-control" name="country" id="country" value="<?php echo $userdetail['country']; ?>" placeholder="Enter Country"
                            required>
                    </div>
                </div>
            </div>

            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputEmail1"><?php echo lang('PIN_CODE'); ?></label>
                        <input type="number" class="form-control" name="pin_code" value="<?php echo $userdetail['pin_code']; ?>" id="pin_code"
                            placeholder="Enter Pincode" required>
                    </div>
                    <div class="col-md-6">
                        <label><?php echo lang('STATUS'); ?></label>
                        <select class="form-control" name="status" id="status">
                            <option value="">---- Select Option ----</option>
                            <option value="active" <?php if($userdetail['status'] == 'active'){echo 'selected' ;} ?>>Active</option>
                            <option value="inactive" <?php if($userdetail['status'] == 'inactive'){echo 'selected' ;} ?>>Inactive</option>
                        </select>
                    </div>
                </div>
            </div>

            <div class="card-footer">
                <center>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </center>
            </div>
        </div>
    </form>
    <?php
include_once "include/footer.php";
?>