<div class="modal fade" id="view-event">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title"><?php echo lang('USER_DETAIL'); ?></h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Name'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['event_name']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Description'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['description']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Start_Date'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['start_date']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_End_Date'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['end_date']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Fees'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['fees']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Max_Contestant'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['max_contestant']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Winning_Prize'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['winning_prize']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Status'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo $eventdetail['status']; ?></div>
                </div>
                <div class="row boderBottom">
                    <div class="col-md-4"><?php echo lang('Event_Created_Date'); ?></div>
                    <div class="col-md-1">:</div>
                    <div class="col-md-5"><?php echo date("d-m-Y",strtotime($eventdetail['created_date']));; ?></div>
                </div>
            </div>
            <div class="modal-footer justify-content-between">
                <!-- <button type="button" class="btn btn-default" data-dismiss="modal">Close</button> -->
                <button type="button" class="btn btn-primary" data-dismiss="modal"><?php echo lang('CLOSE'); ?></button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>