<?php
include_once "include/header.php";
include_once "include/sidebar.php";
?>
<!DOCTYPE html>
<html>

<head>
    <title></title>
</head>

<body>
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <div class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1 class="m-0 text-dark text-bold"><?php echo lang('DASHBOARD'); ?></h1>
                    </div><!-- /.col -->
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href=""><?php echo lang('DASHBOARD'); ?></a></li>

                        </ol>
                    </div><!-- /.col -->
                </div><!-- /.row -->
            </div><!-- /.container-fluid -->
        </div>
        <div class="container">
            <div class="row">
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-info">
                        <div class="inner">
                            <h3> <?php echo $user[0]['total_user']; ?></h3>
                            <p><?php echo lang('TOTAL_USERS'); ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-stats-bars"></i>
                        </div>
                        <a href="<?php echo BASE_URL ?>User/listUser" class="small-box-footer"><?php echo lang('MORE_INFO'); ?><i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-danger">
                        <div class="inner">
                            <h3> <?php echo $event[0]['total_event'] ?></h3>
                            <p><?php echo lang('TOTAL_EVENTS'); ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-person-add"></i>
                        </div>
                        <a href="#" class="small-box-footer"><?php echo lang('MORE_INFO'); ?> <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-warning">
                        <div class="inner">
                            <h3> <?php echo $event_winner[0]['winners'] ?></h3>
                            <p><?php echo lang('TOTAL_EVENT_WINNER'); ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-pie-graph"></i>
                        </div>
                        <a href="#" class="small-box-footer"><?php echo lang('MORE_INFO'); ?> <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
                <div class="col-lg-3 col-6">
                    <div class="small-box bg-success">
                        <div class="inner">
                            <h3> <?php echo $votes[0]['votes'] ?></h3>
                            <p><?php echo lang('TOTAL_VOTES'); ?></p>
                        </div>
                        <div class="icon">
                            <i class="ion ion-bag"></i>
                        </div>
                        <a href="#" class="small-box-footer"><?php echo lang('MORE_INFO'); ?> <i
                                class="fas fa-arrow-circle-right"></i></a>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-primary">
                            <i class="fas fa-unlock"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><?php echo lang("OPEN_EVENTS"); ?></span>
                            <span class="info-box-number"><?php echo $open_events[0]['Events'] ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-warning">
                            <i class="fas fa-history"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><?php echo lang('PREP_EVENTS'); ?></span>
                            <span class="info-box-number"><?php echo $pre_events[0]['Events'] ?></span>
                        </div>
                    </div>
                </div>
                <div class="col-md-3 col-sm-6 col-12">
                    <div class="info-box">
                        <span class="info-box-icon bg-danger">
                            <i class="fas fa-lock"></i>
                        </span>
                        <div class="info-box-content">
                            <span class="info-box-text"><?php echo lang('CLOSE_EVENTS'); ?></span>
                            <span class="info-box-number"><?php echo $close_events[0]['Events'] ?></span>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>

</html>

<?php
include_once "include/footer.php";
?>