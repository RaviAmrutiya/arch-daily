<?php 
$userData = $this->auth->getUserSession();
?>
<!-- Main Sidebar Container -->
<aside class="main-sidebar elevation-4 sidebar-light-primary">
  <!-- Brand Logo -->
  <a href="#" class="brand-link">
    <!-- <img src="dist/img/AdminLTELogo.png" alt="AdminLTE Logo" class="brand-image img-circle elevation-3"
         style="opacity: .8">                                      -->
    <span class="brand-text font-weight-light">Arch Daily</span>
  </a>

  <!-- Sidebar -->
  <div class="sidebar">
    <!-- Sidebar user panel (optional) -->
    <div class="user-panel mt-3 pb-3 mb-3 d-flex">
      <div class="image">
        <img src="<?php echo CON_PROFILES_URL.$userData['0']['profile_pic'] ?>" class="img-circle elevation-2" alt="User Image">
      </div>
      <div class="info">
        <a href="#" class="d-block"><b><?php echo $userData[0]['name'];?></b></a>
      </div>
    </div>

    <!-- Sidebar Menu -->
    <nav class="mt-2">
      <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
        <!-- Add icons to the links using the .nav-icon class
             with font-awesome or any other icon font library -->
          <li class="nav-item">   
           <a href="<?php echo BASE_URL ?>/dashboard" class="nav-link">
              <i class="fas fa-tachometer-alt"></i>
              <p>
                Dashbord
              </p>
            </a>
          </li>
             <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Admin
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Admin</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Admin</p>
              </a>
            </li>
          </ul>
        </li>  
          <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
            Events
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo  BASE_URL ?>Event" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Events</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo  BASE_URL ?>Event/addevent" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Events</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
            Events Winner
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo  BASE_URL ?>EventWinner" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Events Winner</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo BASE_URL ?>EventWinner/addeventwinner" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Events Winner</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              User Details
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo  BASE_URL ?>User/listUser" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List User Details</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo BASE_URL ?>User/adduser" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add User Details</p>
              </a>
            </li>
          </ul>
        </li>
        <li class="nav-item has-treeview">
          <a href="#" class="nav-link">
            <i class="nav-icon fas fa-copy"></i>
            <p>
              Event Participation  
              <i class="fas fa-angle-left right"></i>
            </p>
          </a>
          <ul class="nav nav-treeview">
            <li class="nav-item">
              <a href="<?php echo BASE_URL ?>EventParticipation" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>List Of Event Paticipation</p>
              </a>
            </li>
            <li class="nav-item">
              <a href="<?php echo BASE_URL ?>EventParticipation/joinUserToEvent" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Join User</p>
              </a>
            </li>
            <!-- <li class="nav-item">
              <a href="#" class="nav-link">
                <i class="far fa-circle nav-icon"></i>
                <p>Add Picnic Plans</p>
              </a>
            </li> -->
          </ul>
        </li>
      </ul>
    </nav>
    <!-- /.sidebar-menu -->
  </div>
  <!-- /.sidebar -->
</aside>

<!-- Content Wrapper. Contains page content -->


  <!-- /.content-header -->
