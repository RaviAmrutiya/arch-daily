<?php
include_once "include/header.php";
include_once "include/sidebar.php";
?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark text-bold"><?php echo lang('JOIN_USER'); ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><a
                                href="<?php echo BASE_URL ?>Dashboard"><?php echo lang('DASHBOARD'); ?></a></li>
                        <li class="breadcrumb-item"><?php echo lang('EVENT_PARTICIPATION'); ?></li>
                        <li class="breadcrumb-item"><?php echo lang('JOIN_USER'); ?></li>
                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <!-- <div class="card card-primary">
        <div class="card-header">
            <h3 class="card-title">Add User</h3>
        </div>
    </div> -->
    <form action="<?php echo BASE_URL ?>EventParticipation/joinUserToEvent" method="POST" enctype="multipart/form-data"
        role="form">
        <div class="card-body">

            <div class="form-group">
                <div class='row'>
                    <div class='col-md-6'>
                        <label>User Name</label>
                        <select class="form-control" name="user_id" id="name" required>
                            <option value=""> choose any one</option>
                            <?php if(!empty($userdetails)){ foreach($userdetails as $user){ ?>

                            <option value="<?php echo $user['user_id'] ?>"> <?php echo $user['name'] ?> </option>
                            <?php   }}?>
                        </select>
                    </div>
                    <div class='col-md-6'>
                        <label>Event Name</label>
                        <select class="form-control" name="event_id" id="name" required>
                            <option value=""> choose any one</option>
                            <?php if(!empty($eventdetails)){ foreach($eventdetails as $event){ ?>

                            <option value="<?php echo $event['event_id'] ?>"> <?php echo $event['event_name'] ?>
                            </option>
                            <?php   }}?>
                        </select>
                    </div>
                </div>
            </div>
            <div class="form-group">
                <div class="row">
                    <div class="col-md-6">
                        <label for="exampleInputFile">Main File</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="main_file" class="custom-file-input" accept="image/*"
                                    id="fileinput" required>
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                    <div class='col-md-6'>
                    <label for="exampleInputFile">Sub File</label>
                        <div class="input-group">
                            <div class="custom-file">
                                <input type="file" name="sub_file" class="custom-file-input" accept="image/*"
                                    id="fileinput" required>
                                <label class="custom-file-label" for="exampleInputFile">Choose file</label>
                            </div>
                        </div>
                    </div>
                </div>
            </div>


            <div class="card-footer">
                <center>
                    <button type="submit" class="btn btn-primary">Submit</button>
                </center>
            </div>
        </div>
    </form>

    <?php include_once "include/footer.php" ?>