<?php
include_once "include/header.php";
include_once "include/sidebar.php";

?>
<div class="content-wrapper">
  <!-- Content Header (Page header) -->
  <div class="content-header">
    <div class="container-fluid">
      <div class="row mb-2">
        <div class="col-sm-6">
          <h1 class="m-0 text-dark text-bold">List Event</h1>
        </div><!-- /.col -->
        <div class="col-sm-6">
          <ol class="breadcrumb float-sm-right">
            <li class="breadcrumb-item"><a href="#">Home</a></li>
            <li class="breadcrumb-item active">Dashboard</li>

          </ol>
        </div><!-- /.col -->
      </div><!-- /.row -->
    </div><!-- /.container-fluid -->
  </div>
  <div class="card">
            <div class="card-header">
              <h3 class="card-title">Data Table With Full Features</h3>
            </div>
            <!-- /.card-header -->
            <div class="card-body">
              <table id="eventlist" class="table table-bordered table-striped">
                <thead>
                <tr>
                  <th><?php echo lang("SR_NO"); ?></th>
                  <th><?php echo  lang("Event_Name"); ?></th>
                  <!-- <th><?php echo  lang("Event_Description"); ?></th> -->
                  <th><?php echo lang("Event_Start_Date"); ?></th>
                  <th><?php echo lang("Event_End_Date") ?></th>
                  <th><?php echo lang("Event_Fees");  ?></th>
                  <!-- <th><?php echo lang("Event_Max_Contestant"); ?></th>
                  <th><?php echo lang("Winning_Prize"); ?></th> -->
                  <th><?php echo lang("Event_Status"); ?></th>
                  <th><?php echo lang("Action");  ?></th>
                </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
            </div>
            <!-- /.card-body -->
          </div>
          <div class="viewevent"></div>
<?php
include_once "include/footer.php";
?>



<script type="text/javascript">

$(document).ready(function() {

    $('#eventlist').DataTable({
      "paging": true,
      "lengthChange": false,
      "searching": true,
      "ordering": true,
      "columns": [
        null,
        null,
        null,
        null,
        null,
        null,
        {
          "orderable": false
        }
      ],
      "info": true,
      "autoWidth": false,
      "processing": true,
      "serverSide": true,
      "ajax": {
        url: "<?php echo base_url("Event/listEvent") ?>",
        type: 'POST'
      }
    });


    //View Event
    $(document).on("click", "#view", function(e) {
        e.preventDefault();
          var id = $(this).attr('data-id');
          var url = $(this).attr("target");
          var data_div = $(this).attr("data-div-target");
         
          $.ajax({
            url: url,
            type: 'POST',
            data:{id : id},
            success:function(msg){    
                console.log(msg);
                            
              $(".viewevent").html(msg);
              $("#view-event").modal({
                backdrop:'static',
                keyboard:true
              });
              $('#view-event').modal('show');
            }
          });
        });

    //Delete Event Record
    $(document).on("click", "#delete", function() {
          var id = $(this).attr('data-id');          
          $.ajax({
            url: "<?php echo base_url("Event/deleteEvent") ?>",
            type: 'POST',
            data:{id : id},
            success:function(data){
              window.location.reload();
              toastr.success('Deleted');
            }

          });
        });
});   

</script>
