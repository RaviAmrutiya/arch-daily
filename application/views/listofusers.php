<?php
include_once "include/header.php";
include_once "include/sidebar.php";

?>
<div class="content-wrapper">
    <!-- Content Header (Page header) -->
    <div class="content-header">
        <div class="container-fluid">
            <div class="row mb-2">
                <div class="col-sm-6">
                    <h1 class="m-0 text-dark text-bold"><?php echo lang('USER_LIST'); ?></h1>
                </div><!-- /.col -->
                <div class="col-sm-6">
                    <ol class="breadcrumb float-sm-right">
                        <li class="breadcrumb-item"><?php echo lang('DASHSBOARD'); ?></li>
                        <li class="breadcrumb-item"><?php echo lang('USER'); ?></li>
                        <li class="breadcrumb-item"><a href=""><?php echo lang('USER_LIST'); ?></a></li>


                    </ol>
                </div><!-- /.col -->
            </div><!-- /.row -->
        </div><!-- /.container-fluid -->
    </div>
    <div class="card">
        <div class="card-header">
            <h3 class="card-title"><?php echo lang('CARD_TITLE'); ?></h3>
        </div>
        <!-- /.card-header -->
        <div class="card-body">
            <table id="userList" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th><?php echo lang("SR_NO"); ?></th>
                        <th><?php echo lang("NAME"); ?></th>
                        <th><?php echo lang("EMAIL"); ?></th>
                        <th><?php echo lang("MOBILE"); ?></th>
                        <th><?php echo lang("STATUS"); ?></th>
                        <th><?php echo lang("ACTIONS"); ?></th>
                    </tr>
                </thead>
                <tbody>

                </tbody>
            </table>
        </div>
        <!-- /.card-body -->
    </div>
    <div class="viewusermodal"></div>
    <?php
include("include/footer.php");
?>

    <script type="text/javascript">
    $(document).ready(function() {

        $('#userList').DataTable({
            "paging": true,
            "lengthChange": false,
            "searching": true,
            "ordering": true,
            "columns": [{
                    "orderable": false
                },
                null,
                null,
                null,
                null,
                {
                    "orderable": false
                }
            ],
            "info": true,
            "autoWidth": false,
            "processing": true,
            "serverSide": true,
            "ajax": {
                url: "<?php echo base_url("User/listOFUser ") ?>",
                type: 'POST'
            }
        });

       //View user
        $(document).on("click", "#view", function(e) {
        e.preventDefault();
          var id = $(this).attr('data-id');
          var url = $(this).attr("target");
          var data_div = $(this).attr("data-div-target");
         
          $.ajax({
            url: url,
            type: 'POST',
            data:{id : id},
            success:function(msg){    
                console.log(msg);
                            
              $(".viewusermodal").html(msg);
              $("#view-modal").modal({
                backdrop:'static',
                keyboard:true
              });
              $('#view-modal').modal('show');
            }
          });
        });

        //deleting record
        $(document).on("click", "#delete", function() {
          var id = $(this).attr('data-id');          
          $.ajax({
            url: "<?php echo base_url("User/deleteUSer") ?>",
            type: 'POST',
            data:{id : id},
            success:function(data){
                toastr.success('deleted');
              window.location.reload();
            }

          });
        });
    
    });
    </script>