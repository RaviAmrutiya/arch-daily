<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Contest_Model extends MY_Model
{
    public function __construct()
    {
        parent::__construct();
    }

    /*  login */

    public function listOfContest($searchText, $order_by, $sort_by, $offset, $limit)
    {
        $offset_limit = "";
        if (!empty($limit)) {
            $offset_limit = "LIMIT {$offset},{$limit}";
        }
        $order_by = "ORDER BY {$order_by} {$sort_by}";
        $like = "";
        if (!empty($searchText)) {
            $like = " AND (t1.contest_name LIKE '%{$searchText}%' OR t1.vote_open_date LIKE '%{$searchText}%' OR t1.vote_close_date LIKE '%{$searchText}%')";
        }

        $sql = "SELECT t1.*, IFNULL((SELECT count(*) FROM tbl_contestant_mapping  tm INNER JOIN tbl_contestant_details t2 ON tm.contestant_id=t2.contestant_id WHERE t2.status != 'deleted' AND tm.contest_id = t1.contest_id),0) as user_count, 
        IFNULL((SELECT sum(vote) FROM tbl_voting WHERE contest_id = t1.contest_id), 0) as total_vote 
        FROM tbl_contest t1 
        WHERE t1.status != 'delete'
        {$like} {$order_by} {$offset_limit}";

        $result = $this->db->query($sql);

        return $this->returnRows($result);
    }

    public function addContest($contestData)
    {
        return $this->insert('tbl_contest', $contestData);
    }

    public function getContestant($contest_id)
    {
        $sql = "SELECT COUNT(contestant_id) AS Total_contestant FROM tbl_contestant_mapping WHERE contest_id={$contest_id}";
        $result = $this->db->query($sql);
        return $this->returnRows($result);
    }

    public function addMapping($mappingData)
    {
        return $this->insert('tbl_contestant_mapping', $mappingData);
    }
    
    public function getListOfContestant($contest_id)
    {
        $sql="SELECT t1.contestant_id,t1.name FROM tbl_contestant_details t1 WHERE t1.contestant_id NOT IN (SELECT contestant_id FROM tbl_contestant_mapping WHERE contest_id !={$contest_id}) AND t1.status='active'
        ";
        $result = $this->db->query($sql);
       return $this->returnRows($result);
    }
   
}
