<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api extends MY_Controller
{

    public $resArr = array(
        CON_RES_CODE => CON_CODE_FAIL,
        CON_RES_MESSAGE => "",
        CON_RES_DATA => array(),
    );
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        $this->setLanguage($this->input->post_get('language'));
        $this->load->model('Api_Model', 'APIModel');
    }

    /* ------------------------------- Index ------------------------------- */
    public function index()
    {
        $this->resArr[CON_RES_MESSAGE] = lang('MSG_ACCESS_DENIED');
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- Under Construction ------------------------------- */
    public function underConstruction()
    {
        $this->load->view('under_construction');
    }

    /* ------------------------------- Terms & condition for sign up ------------------------------- */
    public function termsAndCondSignUp()
    {
        $this->load->view('terms_and_conditions_sign_up');
    }

    /* ------------------------------- Privacy policy for sign up ------------------------------- */
    public function privacyPolicyForSignUp()
    {
        $this->load->view('privacy_policy_for_sign_up');
    }

    /* ------------------------------- Under Construction ------------------------------- */
    public function contestWebView()
    {
        $contest_id = $this->input->get('contest_id');
        $data['contestDetails'] = $this->APIModel->getContestDetailsById($contest_id);
        $this->load->view('home_page_preview', $data);
    }

    /* ------------------------------- Home banner preview ------------------------------- */
    // public function homePagePreview()
    // {
    //     $contest_id = $this->input->get('contest_id');
    //     $data['contestDetails'] = $this->APIModel->getContestDetailsById($contest_id);
    //     $this->load->view('home_page_preview', $data);
    // }

    /* ------------------------------- Login ------------------------------- */
    public function login()
    {
        //Fetch Request Parameter
        $emailOrPhone = $this->input->post('email_or_phones');
        $password = $this->input->post('password');

        if (!empty($emailOrPhone) && !empty($password)) {
            //Check Login
            $loginData = $this->APIModel->checkLogin($emailOrPhone, $password);

            if (!empty($loginData)) {

                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LOGIN_SUCCESS');
                $this->resArr[CON_RES_DATA] = array('profile_details' => $loginData);
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LOGIN_FAIL');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- Sign UP ------------------------------- */
    public function signup()
    {
        //Fetch Request Parameter
        $name = $this->input->post('name');
        $user_name = $this->input->post('user_name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $gender = $this->input->post('gender');
        $company_name = $this->input->post('company_name');
        $web_url = $this->input->post('web_url');
        $profession = $this->input->post('profession');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $country = $this->input->post('country');
        $pin_code = $this->input->post('pin_code');
        $status = 'inactive';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        if (!empty($email) && !empty($password) && !empty($mobile) && !empty($name) && !empty($name)) {

            // //check for unique user name
            // $whrArrUnique = array('user_name' => $user_name, 'status !=' => 'deleted');
            // $checkUserNameExists = $this->APIModel->getByWhere('tbl_user_details', $whrArrUnique);
            // if (empty($checkUserNameExists)) {

            //Check Email Already Exists
            $whereArr = array('email' => $email, 'status !=' => 'deleted');
            $checkEmailExists = $this->APIModel->getByWhere('tbl_user_details', $whereArr);
            if (empty($checkEmailExists)) {

                //Check Mobile Already Exists
                $whereArr = array('mobile' => $mobile, 'status !=' => 'deleted');
                $checkMobileExists = $this->APIModel->getByWhere('tbl_user_details', $whereArr);
                if (empty($checkMobileExists)) {
                    // insert user data
                    $userDataArr = array(
                        'name' => $name,
                        'user_name' => $user_name,
                        'mobile' => $mobile,
                        'email' => $email,
                        'password' => md5($password),
                        'gender' => $gender,
                        'web_url' => $web_url,
                        'company_name' => $company_name,
                        'profession' => $profession,
                        'address' => $address,
                        'city' => $city,
                        'state' => $state,
                        'country' => $country,
                        'status' => $status,
                        'created_date' => $created_date,
                        'updated_date' => $updated_date,
                    );
                    if(!empty($pin_code)){
                        $userDataArr['pincode'] = $pin_code;
                    }
                    $user_id = $this->APIModel->insert('tbl_user_details', $userDataArr);
                    $useArr = array('user_id' => $user_id);
                    $userDetail = $this->APIModel->getByWhere('tbl_user_details', $userDataArr);
                    if ($user_id > 0) {
                        $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                        $this->resArr[CON_RES_MESSAGE] = lang('MSG_SIGNUP_SUCCESS');
                        $this->resArr[CON_RES_DATA] = array('profile_details' => $userDetail);
                    } else {
                        $this->resArr[CON_RES_MESSAGE] = lang('MSG_SIGNUP_FAIL');
                    }
                } else {
                    $this->resArr[CON_RES_MESSAGE] = lang('MSG_MOBILE_EXISTS');
                }
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EMAIL_EXISTS');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        //  else {
        // $this->resArr[CON_RES_MESSAGE] = lang('MSG_USERNAME_EXISTS');
        // }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of Event ------------------------------- */

    public function listOfEvent()
    {
        $list = $this->APIModel->listEvent();
        foreach ($list as $key => $value) {
            $list[$key]['status'] = lang($value['status']);
        }

        if (!empty($list)) {
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT');
            $this->resArr[CON_RES_DATA] = array('list_event' => $list);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT_EMPTY');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- Event detail ------------------------------- */

    public function eventDetail()
    {
        $resArr = array();
        $event_id = $this->input->post('event_id');
        if (!empty($event_id)) {
            $event_detail = $this->APIModel->detailEvent($event_id);
            foreach ($event_detail as $key => $value) {
                $event_detail[$key]['status'] = lang($value['status']);
            }
            $resArr['event_detail'] = $event_detail;
            if (!empty($event_detail)) {
                $user_detail = $this->APIModel->getUserOfEvent($event_id);
                $resArr['user_list'] = $user_detail;
                foreach ($resArr['user_list'] as $key => $value) {
                    $totalVote = $this->APIModel->getVoteCount($event_id, $resArr['user_list'][$key]['user_id']);
                    $resArr['user_list'][$key]['total_vote'] = $totalVote[0]['total_vote'];
                }

                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EVENT_DETAIL');
                $this->resArr[CON_RES_DATA] = $resArr;
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EVENT_EMPTY');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }

        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- Add contestant to mapping  ------------------------------- */

    public function addToMapping()
    {
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');
        $status = 'inactive';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        $whereArr = array('event_id' => $event_id, 'user_id' => $user_id);
        $checkIdExists = $this->APIModel->getByWhere('user_mapping', $whereArr);
        if (empty($checkIdExists)) {

            $list = $this->APIModel->getdate($event_id, $created_date);
            // print_r($list);exit();
            if (!empty($list)) {
                $mappingArr = array(
                    'event_id' => $event_id,
                    'user_id' => $user_id,
                    'status' => $status,
                    'created_date' => $created_date,
                    'updated_date' => $updated_date,
                );

                $add = $this->APIModel->insert('user_mapping', $mappingArr);
                if ($add > 0) {
                    $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                    $this->resArr[CON_RES_MESSAGE] = lang('MSG_ADD_MAPPING');
                    $this->resArr[CON_RES_DATA] = array();
                } else {
                    $this->resArr[CON_RES_MESSAGE] = lang('MSG_ADD_MAPPING_FAIL');
                }
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_CANT_JOIN');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_USER_EXISTS');
        }

        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- Edit User  ------------------------------- */

    public function editUser()
    {
        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        //  $password = $this->input->post('password');
        $gender = $this->input->post('gender');
        $company_name = $this->input->post('company_name');
        $web_url = $this->input->post('web_url');
        $profession = $this->input->post('profession');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $country = $this->input->post('country');
        $pin_code = $this->input->post('pin_code');

        $whereArrUser = array('user_id' => $user_id);
        $editArr = array();
        $checkEmailExists = array();
        $checkMobileExists = array();

        if (!empty($email)) {
            //Check Email Already Exists
            $whereArr = array('user_id !=' => $user_id, 'email' => $email, 'status !=' => 'deleted');
            $checkEmailExists = $this->APIModel->getByWhere('tbl_user_details', $whereArr);
        }
        if (empty($checkEmailExists)) {

            if (!empty($mobile)) {
                //check mobile exists
                $whereArr = array('user_id !=' => $user_id, 'mobile' => $mobile, 'status !=' => 'deleted');
                $checkMobileExists = $this->APIModel->getByWhere('tbl_user_details', $whereArr);
            }
            if (empty($checkMobileExists)) {
                if (!empty($email)) {
                    $editArr['email'] = $email;
                }
                if (!empty($mobile)) {
                    $editArr['mobile'] = $mobile;
                }
                if (!empty($name)) {
                    $editArr['name'] = $name;
                }
                if (!empty($gender)) {
                    $editArr['gender'] = $gender;
                }
                if (!empty($company_name)) {
                    $editArr['company_name'] = $company_name;
                }
                if (!empty($web_url)) {
                    $editArr['web_url'] = $web_url;
                }
                if (!empty($profession)) {
                    $editArr['profession'] = $profession;
                }
                if (!empty($address)) {
                    $editArr['address'] = $address;
                }
                if (!empty($city)) {
                    $editArr['city'] = $city;
                }
                if (!empty($state)) {
                    $editArr['state'] = $state;
                }
                if (!empty($country)) {
                    $editArr['country'] = $country;
                }
                if (!empty($pin_code)) {
                    $editArr['pin_code'] = $pin_code;
                }

                $editUser = $this->APIModel->update('tbl_user_details', $editArr, $whereArrUser);
               
                    $userDetail = $this->APIModel->updatedUser($user_id);
                    $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                    $this->resArr[CON_RES_MESSAGE] = lang('MSG_EDIT_SUCCESS');
                    $this->resArr[CON_RES_DATA] = array('profile_details' => $userDetail);
               
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_MOBILE_EXISTS');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_EMAIL_EXISTS');
        }

        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- change profile picture ------------------------------- */

    public function changeProfilePic()
    {
        $user_id = $this->input->post('user_id');
        $whrArr = array('user_id' => $user_id);
        $updateData = array();
        if (!empty($_FILES['profile_pic']['name'])) {
            $config['upload_path'] = CON_PROFILES_PATH;
            $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
            $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
            $filename = $user_id . "_" . time() . "." . $ext;
            $config['file_name'] = $filename;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
            if (!$this->upload->do_upload('profile_pic')) {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_FILE_UPLOAD_FAIL');
                $this->sendResponse($this->resArr);
            } else {
                // $main_image = $filename;
                $fileData = $this->upload->data();
                $main_image = $fileData['file_name'];
            }
            if (!empty($main_image)) {

                $updateData['profile_pic'] = $main_image;
                $updateData['profile_pic_path'] = $main_image;
            }
            $editProfile = $this->APIModel->update('tbl_user_details', $updateData, $whrArr);
            if ($editProfile == 1) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EDIT_SUCCESS');
                $this->resArr[CON_RES_DATA] = array();
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EDIT_FAIL');
                $this->resArr[CON_RES_DATA] = array();
            }

        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }

    public function uploadFile()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $status = 'inactive';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');
        if (!empty($user_id) && !empty($event_id)) {
            $addArr = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'status' => $status,
                'created_date' => $created_date,
                'updated_date' => $updated_date,
            );
            if (!empty($_FILES['main_file']['name'])) {
                $config['upload_path'] = CON_MAIN_FILES_PATH;
                $config['allowed_types'] = '*';
                $ext = pathinfo($_FILES['main_file']['name'], PATHINFO_EXTENSION);
                if ($ext == 'dwg') {
                    $filename = $user_id . "_" . $event_id . "_" . time() . "." . $ext;
                    $config['file_name'] = $filename;
                    $config['overwrite'] = true;
                    $this->load->library('upload', $config);
                    if (!$this->upload->do_upload('main_file')) {
                        $this->resArr[CON_RES_MESSAGE] = "main file uploading fail";
                        $this->sendResponse($this->resArr);
                    } else {
                        $fileData = $this->upload->data();
                        $main_image = $fileData['file_name'];
                    }
                    if (!empty($main_image)) {
                        $addArr['main_file'] = $main_image;
                        $addArr['main_file_path'] = $main_image;
                    }
                    if (!empty($_FILES['sub_file']['name'])) {
                        $config1['upload_path'] = CON_SUB_FILES_PATH;
                        $config1['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
                        $ext = pathinfo($_FILES['sub_file']['name'], PATHINFO_EXTENSION);
                        $filename = $user_id . "_" . $event_id . "_" . time() . "." . $ext;
                        $config1['file_name'] = $filename;
                        $config1['overwrite'] = true;
                        $this->upload->initialize($config1);
                        $this->load->library('upload', $config1);
                        if (!$this->upload->do_upload('sub_file')) {
                            $this->resArr[CON_RES_MESSAGE] = lang('MSG_FILE_UPLOAD_FAIL');
                            $this->sendResponse($this->resArr);
                        } else {
                            // $main_image = $filename;
                            $fileData = $this->upload->data();
                            $sub_image = $fileData['file_name'];
                        }
                        if (!empty($sub_image)) {

                            $addArr['sub_file'] = $sub_image;
                            $addArr['sub_file_path'] = $sub_image;
                        }
                    }
                    $addFiles = $this->APIModel->insert('event_files', $addArr);
                    if ($addFiles > 0) {
                        $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                        $this->resArr[CON_RES_MESSAGE] = lang('file_success');
                        $this->resArr[CON_RES_DATA] = array();
                        $this->sendResponse($this->resArr);
                    } else {
                        $this->resArr[CON_RES_MESSAGE] = lang('MSG_FILE_UPLOAD_FAIL');
                        $this->sendResponse($this->resArr);
                    }
                }
                $this->resArr[CON_RES_MESSAGE] = lang('proper_file');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }

        $this->sendResponse($this->resArr);
    }

    public function getUserFiles()
    {
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)) {
            $mainfiles = $this->APIModel->getMainFiles($user_id);
            $subFiles = $this->APIModel->getSubFiles($user_id);
            $totalVote = $this->APIModel->gettotalvote($user_id);
            $totaleventparticipate = $this->APIModel->gettotalparticipate($user_id);
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('file_success');
            $this->resArr[CON_RES_DATA] = array('main_files' => $mainfiles, 'sub_files' => $subFiles, 'User_total_vote' => $totalVote, 'total_event_participate' => $totaleventparticipate);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }

    public function addVote()
    {
        $voter_id = $this->input->post('voter_id');
        $to_vote_id = $this->input->post('to_vote_id');
        $event_id = $this->input->post('event_id');
        $vote = $this->input->post('vote');
        $voted = 'yes';
        $status = 'active';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        if (!empty($voter_id) && !empty($to_vote_id) && !empty($event_id) && !empty($vote)) {
            $whr = array(
                'voter_id' => $voter_id,
                'to_vote_id' => $to_vote_id,
                'event_id' => $event_id,
            );
            $alreadyVoted = $this->APIModel->getByWhere('tbl_voteing', $whr);
            if (empty($alreadyVoted)) {
                $addArr = array(
                    'voter_id' => $voter_id,
                    'to_vote_id' => $to_vote_id,
                    'event_id' => $event_id,
                    'vote' => $vote,
                    'voted' => $voted,
                    'status' => $status,
                    'created_date' => $created_date,
                    'updated_date' => $updated_date,
                );
                $addvote = $this->APIModel->insert('tbl_voteing', $addArr);
                $totalVote = $this->APIModel->getVoteCount($event_id, $to_vote_id);
                if ($addvote) {
                    $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                    $this->resArr[CON_RES_MESSAGE] = lang('vote_done');
                    $this->resArr[CON_RES_DATA] = array('total_votes' => $totalVote);
                } else {
                    $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                    $this->resArr[CON_RES_MESSAGE] = lang('vote_fail');
                }
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('already_voted');

            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);
    }

    public function getUserData()
    {
        $user_id = $this->input->post('user_id');

        if (!empty($user_id)) {
            $userArr = array('user_id' => $user_id);
            $mainfiles = $this->APIModel->getMainFiles($user_id);
            $subFiles = $this->APIModel->getSubFiles($user_id);
            $userDetail = $this->APIModel->getByWhere('tbl_user_details', $userArr);
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('file_success');
            $this->resArr[CON_RES_DATA] = array('user_details' => $userDetail, 'main_files' => $mainfiles, 'sub_files' => $subFiles);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of user ------------------------------- */

    public function listOfUser()
    {
        $list = $this->APIModel->Userdetail();
        foreach ($list as $key => $value) {
            $list[$key]['status'] = lang($value['status']);
        }

        if (!empty($list)) {
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT');
            $this->resArr[CON_RES_DATA] = array('list_user' => $list);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT_EMPTY');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of user in event ------------------------------- */

    public function listOfUserOfEvent()
    {
        $resArr = array();
        $event_id = $this->input->post('event_id');

        $list = $this->APIModel->getUserOfEventList($event_id);

        if (!empty($list)) {
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT');
            $this->resArr[CON_RES_DATA] = array('list_user_event' => $list);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT_EMPTY');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of user detail in event ------------------------------- */

    public function listOfUserdetailOfEvent()
    {
        $resArr = array();
        $event_id = $this->input->post('event_id');
        $user_id = $this->input->post('user_id');

        if (!empty($user_id) && !empty($event_id)) {
            $list = $this->APIModel->getUserdetailOfEventList($event_id, $user_id);
            $subFile = $this->APIModel->getfiledetail($event_id, $user_id);
            $voter = $this->APIModel->getvoterdetail($event_id, $user_id);
            if (!empty($list)) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT');
                $this->resArr[CON_RES_DATA] = array('list_user_event' => $list, 'sub_files' => $subFile, 'voter_detail' => $voter);
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT_EMPTY');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of all sub files ------------------------------- */

    public function allSubFiles()
    {
        $list = $this->APIModel->detailsubfiles();
    

        if (!empty($list)) {
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_SUBFILES');
            $this->resArr[CON_RES_DATA] = array('subfiles_with_user_detail_history' => $list);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_SUBFILES_EMPTY');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of all winner ------------------------------- */

    public function listOfAllWinner()
    {
        $list = $this->APIModel->allwinnerlist();
       
        if (!empty($list)) {
            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_WINNER');
            $this->resArr[CON_RES_DATA] = array('all_winner_list' => $list);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_WINNER_EMPTY');
        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- List Of user winner ------------------------------- */

    public function userWinningList()
    {
        $user_id = $this->input->post('user_id');
        if (!empty($user_id)) {
            $list = $this->APIModel->userwinnerlist($user_id);
            //foreach ($list as $key => $value) {
            //    $list[$key]['status'] = lang($value['status']);
            //}

            if (!empty($list)) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_WINNER');
                $this->resArr[CON_RES_DATA] = array('user_winner_list' => $list);
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_WINNER_EMPTY');
            }
            $this->sendResponse($this->resArr);
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
    }

    /* ------------------------------- List Of voter detail in event ------------------------------- */

    // public function list()
    // {
    //     $resArr = array();
    //     $event_id = $this->input->post('event_id');
    //     $user_id = $this->input->post('user_id');

    //     $list = $this->APIModel->getvoterdetail($event_id,$user_id);
    //     if (!empty($list)) {
    //         $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
    //         $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT');
    //         $this->resArr[CON_RES_DATA] = array('list_voter_detail' => $list,);
    //     } else {
    //         $this->resArr[CON_RES_MESSAGE] = lang('MSG_LIST_EVENT_EMPTY');
    //     }
    //     $this->sendResponse($this->resArr);
    // }

    /* ------------------------------- FUNCTION FOR SENDING EMAIL ------------------------------- */

    public function mail($email,$code)
    {
        $config = Array(
            'smtp_port' => '25 ',

            'smtp_user' => '4foxwebsolution@gmail.com',
            'smtp_pass' => 'greendevil007',
            'mailtype' => 'html',

        );
        $from_email = "4foxwebsolution@gmail.com"; 
        $to_email = $email; 
        $subject ="Account Verification";
        $message = "Thanks for joinning with " . CON_SMTP_FROM_NAME . ".<br>Your account verification code is : <b>" . $code . "</b>";
        //Load email library 
        $this->load->library('email',$config); 
  
        $this->email->from($from_email, 'Arch Daily'); 
        $this->email->to($to_email);
        $this->email->subject($subject); 
        $this->email->message($message); 
 
        //Send mail 
        if($this->email->send()) 
            return $code;
        else 
            return 0;
        // $data['mail_to'] = $email;
        // $data['mail_subject'] = CON_SMTP_FROM_NAME;
        // $data['mail_message'] = "Thanks for joinning with " . CON_SMTP_FROM_NAME . ".<br>Your account verification code is : <b>" . $code . "</b>";
        // $this->load->library('Mail', $data);
        // // $a = $this->mail->sendMail();
        // // print_r($a);exit();
        // if (!$this->mail->sendMail()) {
        //     return 0;
        // } else {
        //     return $code;
        // }
    }

        /* ------------------------------- ADD SUB FILES ------------------------------- */


    public function addSubFile()
    {
        $user_id = $this->input->post('user_id');
        $event_id = $this->input->post('event_id');
        $status = 'active';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');
        if (!empty($user_id) && !empty($event_id)) {
            $addArr = array(
                'user_id' => $user_id,
                'event_id' => $event_id,
                'status' => $status,
                'created_date' => $created_date,
                'updated_date' => $updated_date,
            );
            if (!empty($_FILES['sub_file']['name'])) {
                $config['upload_path'] = CON_SUB_FILES_PATH;
                $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
                $ext = pathinfo($_FILES['sub_file']['name'], PATHINFO_EXTENSION);
                $filename = $user_id . "_" . $event_id . "_" . time() . "." . $ext;
                $config['file_name'] = $filename;
                $config['overwrite'] = true;
                $this->load->library('upload', $config);
                if (!$this->upload->do_upload('sub_file')) {
                    $this->resArr[CON_RES_MESSAGE] = lang('MSG_FILE_UPLOAD_FAIL');
                    $this->sendResponse($this->resArr);
                } else {
                    // $main_image = $filename;
                    $fileData = $this->upload->data();
                    $sub_image = $fileData['file_name'];
                }
                if (!empty($sub_image)) {

                    $addArr['sub_file'] = $sub_image;
                    $addArr['sub_file_path'] = $sub_image;
                }
            }
            $addFiles = $this->APIModel->insert('event_files', $addArr);
            if ($addFiles > 0) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('file_success');
                $this->resArr[CON_RES_DATA] = array();
                $this->sendResponse($this->resArr);
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_FILE_UPLOAD_FAIL');
                $this->sendResponse($this->resArr);
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }

        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- CHECK FOR VALID EMAIL ------------------------------- */

    public function CheckMailExists()
    {
        $Email = $this->input->post('email');
        if (!empty($Email)) {
            $whr = array('email' => $Email, 'status !=' => 'deleted');
            $result = $this->APIModel->getByWhere('tbl_user_details', $whr);
            if (empty($result)) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('VALID_EMAIL');
                $this->resArr[CON_RES_DATA] = array();
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_EMAIL_EXISTS');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- CHECK FOR VALID MOBILE ------------------------------- */

    public function CheckMoileExists()
    {
        $mobile = $this->input->post('mobile');
        if (!empty($mobile)) {
            $whr = array('mobile' => $mobile, 'status !=' => 'deleted');
            $result = $this->APIModel->getByWhere('tbl_user_details', $whr);
            if (empty($result)) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('VALID_MOBILE');
                $this->resArr[CON_RES_DATA] = array();
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_MOBILE_EXISTS');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);
    }

    /* ------------------------------- CHECK FOR VALID USERNAME ------------------------------- */

    public function CheckUsernameExists()
    {
        $user_name = $this->input->post('user_name');
        if (!empty($user_name)) {
            $whr = array('user_name' => $user_name, 'status !=' => 'deleted');
            $result = $this->APIModel->getByWhere('tbl_user_details', $whr);
            if (empty($result)) {
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('VALID_USER');
                $this->resArr[CON_RES_DATA] = array();
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_USERNAME_EXISTS');
            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);
    }

        /* ------------------------------- GET OTP ------------------------------- */

    public function getOtp()
    {
        $email = $this->input->post('email');
        $code = mt_rand(100000, 999999); // Random 6 digit code
        $otp = 1;
        if (!empty($email)) {
            $whr = array('email' => $email, 'status !=' => 'deleted');
            $result = $this->APIModel->getByWhere('tbl_user_details', $whr);
            if (!empty($result)) {
                $user_id = $result[0]['user_id'];
                $otp = $this->mail($email,$code);
                
                
                if ($otp !== 0) {
                    $otpFor = 'email';
                    $status = 'unverified';
                    $created_date = date('Y-m-d H:i:s');
                    $updated_date = date('Y-m-d H:i:s');
                    $insrtArr = array(
                        'user_id' => $user_id,
                        'otp' => $code,
                        'otp_for' => $otpFor,
                        'status' => $status,
                        'created_date' => $created_date,
                        'updated_date' => $updated_date,

                    );
                    $insertOtp = $this->APIModel->insert('tbl_otp', $insrtArr);
                    if ($insertOtp > 0) {
                        $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                        $this->resArr[CON_RES_MESSAGE] = lang('OTP_DONE');
                        $this->resArr[CON_RES_DATA] = array('user_id' => $user_id, 'otp' => $code);
                    } else {
                        $this->resArr[CON_RES_MESSAGE] = lang('OTP_FAIL');

                    }
                } else {
                    $this->resArr[CON_RES_MESSAGE] = lang('EMAIL_FAIL');

                }
            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('MSG_SIGNUP_FAIL');
            }

        } else {

            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);

    }

        /* ------------------------------- VERIFY OTP ------------------------------- */


    public function VerifyOtp()
    {
        $user_id = $this->input->post('user_id');
        $Entered_otp = $this->input->post('otp');
        $otpFor = $this->input->post('otp_for');
        $balance = '1000';
        $status = 'active';
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        if (!empty($user_id) && !empty($Entered_otp) && !empty($otpFor) && !empty($balance)) {
            $getOtp = $this->APIModel->getDataForotp($user_id, $otpFor);
            if (!empty($getOtp)) {
                $fetched_otp = $getOtp[0]['otp'];
                if ($Entered_otp == $fetched_otp) {
                    $whrArrOtp = array('user_id' => $user_id, 'otp' => $Entered_otp, 'otp_for' => $otpFor);
                    $stausArr = array('status' => 'verified');
                    $updateData = $this->APIModel->update('tbl_otp', $stausArr, $whrArrOtp);
                    if ($updateData == 1) {
                        $whrUser = array('user_id' => $user_id);
                        $UserStatus = array('status' => 'active');
                        $updateUser = $this->APIModel->update('tbl_user_details', $UserStatus, $whrUser);

                        $existBalane = $this->APIModel->getByWhere('tbl_balance',$whrUser);
                        if(empty($existBalane)){
                            $balaceInsr = array(
                                'user_id' => $user_id,
                                'balance' => $balance,
                                'status'=> $status,
                                'created_date' => $created_date,
                                'updated_date' => $updated_date
                            );
                            $insert = $this->APIModel->insert('tbl_balance',$balaceInsr);
                        }
                            $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                            $this->resArr[CON_RES_MESSAGE] = lang('OTP_VERIFIED');
                            $this->resArr[CON_RES_DATA] = array();
                    }
                } else {
                    $this->resArr[CON_RES_MESSAGE] = lang('OPT_MISMATCH');
                }

            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('USER_NOT_FOUND');

            }
        } else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);

    }

        /* ------------------------------- FORGOT PASSWORD ------------------------------- */


    public function ForgotPassword(){
        $user_id = $this->input->post('user_id');
        $new_pass = $this->input->post('new_password');
        if(!empty($user_id) && !empty($new_pass)){
            $whrArr= array('user_id'=>$user_id);
            $updateData = array('password'=> md5($new_pass));
            $updatePass = $this->APIModel->update('tbl_user_details',$updateData,$whrArr);
            if($updatePass == 1){
                $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                $this->resArr[CON_RES_MESSAGE] = lang('PASSWORD_CHANGE');
                $this->resArr[CON_RES_DATA] = array();
                

            }else{
                $this->resArr[CON_RES_MESSAGE] = lang('WORD_CHANGE_FAIL');
            }

        }
        else {
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');

        }
        $this->sendResponse($this->resArr);

    }

        /* ------------------------------- CHNAGE PASSWORD ------------------------------- */


    public function ChangePassword(){
        $user_id = $this->input->post('user_id');
        $old_password=$this->input->post('old_password');
        $new_password = $this->input->post('new_password');
        if(!empty($user_id) && !empty($old_password) && !empty($new_password)){
            $whrArr = array('user_id'=> $user_id,'status'=>'active');
            $userData = $this->APIModel->getByWhere('tbl_user_details',$whrArr);
            if(!empty($userData)){
                if(md5($old_password) == $userData[0]['password']){
                    $updatePass = array('password'=>md5($new_password));
                    $updateUserPass = $this->APIModel->update('tbl_user_details',$updatePass,$whrArr);
                    if($updateUserPass == 1){
                        $this->resArr[CON_RES_CODE] = CON_CODE_SUCCESS;
                        $this->resArr[CON_RES_MESSAGE] = lang('PASSWORD_CHANGE');
                        $this->resArr[CON_RES_DATA] = array();
                    }

                }else{
                    $this->resArr[CON_RES_MESSAGE] = lang('WRONG_OLD_PASS');

                    
                }

            } else {
                $this->resArr[CON_RES_MESSAGE] = lang('USER_NOT_FOUND');

            }

        }else{
            $this->resArr[CON_RES_MESSAGE] = lang('MSG_INVALID_PARAMETER');
        }
        $this->sendResponse($this->resArr);
    }
}
