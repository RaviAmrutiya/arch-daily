<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventParticipation extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        if (!$this->auth->isLogin()) {
            redirect(CON_LOGIN_PATH);
        }
        $this->setWebLanguage();

        $this->load->model('Event_Participation_Model','EPM');
    }

    public function index()
    {
        $this->load->view('listEventParticipation.php');
    }

    public function joinUserToEvent(){
         
        if($this->input->post()){
            $user_id = $this->input->post('user_id');
            $event_id = $this->input->post('event_id');
            
        }


        $whr = array('status'=>'active');
        $where = array('status !=' => 'close');
        $getuserdata = $this->EPM->getByWhere('tbl_user_details',$whr);
        $geteventdata = $this->EPM->getByWhere('event',$where);
        $data['eventdetails']= $geteventdata;
        $data['userdetails']=$getuserdata;

        $this->load->view('joinUserView.php',$data);
    }

   
}
?>