<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        if (!$this->auth->isLogin()) {
            redirect(CON_LOGIN_PATH);
        }
        $this->setWebLanguage();

        $this->load->model('Event_Model','EM');
    }

    public function index()
    {
        $this->load->view('listevent.php');
    }

    public function addevent()
    {
        $this->load->view('addevent.php');
    }

    public function insertEvent()
    {
        $eventname = $this->input->post('eventname');
        $mainbanner = $_FILES['mainbanner']['name'];
        $discription = $this->input->post('discription');
        $startdate = $this->input->post('startdate');
        $enddate = $this->input->post('enddate');
        $fees = $this->input->post('fees');
        $maxcontestant = $this->input->post('maxContestant');
        $winningprize = $this->input->post('winningprize');
        $status = $this->input->post('status');
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        $config['upload_path'] = CON_BANNER_PATH;
        $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
        $ext = pathinfo($_FILES['mainbanner']['name'], PATHINFO_EXTENSION);
        $filename = time() . "." . $ext;
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('mainbanner')) {
            $this->session->set_flashdata('error_msg', lang('FILE_UPLOAD_FAIL'));
            redirect(BASE_URL . 'Event/addevent');
        } else {
            $fileData = $this->upload->data();
            $main_image = $fileData['file_name'];
        }

        $eventdataarr = array(
            'event_name' => $eventname,
            'main_banner' => $main_image,
            'main_banner_path' => $main_image,
            'description' => $discription,
            'start_date' => $startdate,
            'end_date' => $enddate,
            'fees' => $fees,
            'max_contestant' => $maxcontestant,
            'winning_prize' => $winningprize,
            'status' => $status,
            'created_date' => $created_date,
            'updated_date' => $updated_date,
        );
        $event_id = $this->EM->insert('event', $eventdataarr);
        if ($event_id > 0) {
            $this->session->set_flashdata('success_msg', lang('EVENT_ADDED'));
            redirect(BASE_URL . 'Event');
        } else {
            $this->session->set_flashdata('error_msg', lang('EVENT_FAIL'));
            redirect(BASE_URL . 'Event/addevent');
        }
    }
    public function listEvent()
    {

        $sortColumn = ['event_id','event_name', 'start_date','end_date', 'fees', 'status','Action'];

        // print_r($sortColumn); exit();
        $draw = intval($this->input->post("draw"));
        $offset = intval($this->input->post("start"));
        $limit = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $order = $this->input->post("order");


        $searchText = "";
        if (!empty($search['value'])) {
            $searchText = $search['value'];
        }


        $column = $order[0]['column'];
        $sort_by = $order[0]['dir'];
        $order_by = $sortColumn[$column];

        $allrecord = $this->EM->getAll('event');
        $count = $offset;
        $listofevent = $this->EM->listeventdata($searchText, $order_by, $sort_by, $offset, $limit);
        $data = array();
        foreach ($listofevent  as $event) {
            $count++ ;
            $data[] = array(
                $count,
                $event['event_name'],
                date('Y-m-d',strtotime($event['start_date'])),
                date('Y-m-d',strtotime($event['end_date'])),
                $event['fees'],
                $event['status'],
                '<button class="btn btn-primary paddingButton" id="view" title="View" target="'. BASE_URL.'Event/viewEvent" data-toggle="modal"  data-div-target="view-modal" data-id="'.$event['event_id'].'"><i class="fa fa-eye" aria-hidden="true"></i></button> 
                <a href="'.BASE_URL.'Event/getEventForEdit/'.$event['event_id'].'"><button class="btn btn-warning paddingButton" id="edit" title="Edit"><i class="fa fa-pencil-alt" aria-hidden="true"></i></button></a> 
                  <button class="btn btn-danger paddingButton" id="delete" title="delete" data-id="'.$event['event_id'].'"><i class="fa fa-trash" aria-hidden="true"></i></button> ',
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($allrecord),
            "recordsFiltered" => count($allrecord),
            "data" => $data
        );


        echo json_encode($output);
        exit();

    }

    public function deleteEvent(){
        $event_id = $this->input->post('id');
        $whr = array('event_id' => $event_id );
       $delete =  $this->EM->delete('event',$whr);
         
    }

    public function viewEvent(){
        $event_id = $this->input->post('id');
        $whr = array('event_id' => $event_id);
        $eventdata = $this->EM->getByWhere('event',$whr);
        $data['eventdetail'] = $eventdata[0];
        $this->load->view('eventview.php',$data);
    }

    public function getEventForEdit(){
        $event_id = $this->uri->segment(4);
        $whr = array('event_id' => $event_id);
        $eventData = $this->EM->getByWhere('event',$whr);
        $data['eventdetail'] = $eventData[0];
        $this->load->view('eventeditview.php',$data);

    }

    public function editEvent(){

        $event_id = $this->input->post('event_id');
        $event_name = $this->input->post('eventname');
        $description = $this->input->post('description');
        $start_date = $this->input->post('startdate');
        $end_date = $this->input->post('enddate');
        $fees = $this->input->post('fees');
        $max_contestant = $this->input->post('maxContestant');
        $winning_prize = $this->input->post('winningprize');
        $status = $this->input->post('status');
        $whr = array('event_id' => $event_id);

        $eventdataarr = array(
            'event_name' => $event_name,
            'description' => $description,
            'start_date' => $start_date,
            'end_date' => $end_date,
            'fees' => $fees,
            'max_contestant' => $max_contestant,
            'winning_prize' => $winning_prize,
            'status' => $status
        );
        if(!empty($_FILES['profile_pic']['name'])){
            $config['upload_path'] = CON_PROFILES_PATH;
            $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
            $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
            $filename = time() . "." . $ext;
            $config['file_name'] = $filename;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
    
            if (!$this->upload->do_upload('profile_pic')) {
                $this->session->set_flashdata('error_msg', lang('MSG_FILE_UPLOAD_FAIL'));
            redirect(BASE_URL . 'Event/getEventForEdit/'.$event_id);
            } else {

                $fileData = $this->upload->data();
                $main_image = $fileData['file_name'];
                $userdataarr['profile_pic']= $main_image;
            }

        }
        $update = $this->EM->update('event' , $eventdataarr,$whr);
        if($update == 1){
            $this->session->set_flashdata('success_msg', lang('MSG_EDIT_SUCCESS'));
        }else{
            $this->session->set_flashdata('error_msg', lang('MSG_EDIT_FAIL'));

        }
        redirect(BASE_URL . 'Event');
        
    }

}
