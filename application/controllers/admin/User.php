<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        $this->setWebLanguage();

        $this->load->model('User_Model', 'UM');
    }

    public function index()
    {
        $this->load->view('listuser.php');
    }

    public function adduser()
    {
        $this->load->view('adduser.php');
    }

    public function insertUser()
    {
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $password = $this->input->post('password');
        $profilepic = $_FILES['profile_pic']['name'];
        $gender = $this->input->post('gender');
        $weburl = $this->input->post('web_url');
        $companyname = $this->input->post('company_name');
        $profession = $this->input->post('profession');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $country = $this->input->post('country');
        $pincode = $this->input->post('pin_code');
        $status = $this->input->post('status');
        $created_date = date('Y-m-d H:i:s');
        $updated_date = date('Y-m-d H:i:s');

        $config['upload_path'] = CON_PROFILES_PATH;
        $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
        $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
        $filename = time() . "." . $ext;
        $config['file_name'] = $filename;
        $config['overwrite'] = true;
        $this->load->library('upload', $config);

        if (!$this->upload->do_upload('profile_pic')) {
            $this->session->set_flashdata('error_msg', lang('MSG_FILE_UPLOAD_FAIL'));
            redirect(BASE_URL . 'User/adduser');
        } else {

            $fileData = $this->upload->data();
            $main_image = $fileData['file_name'];
        }

        $userdataarr = array(

            'name' => $name,
            'user_name' => $username,
            'mobile' => $mobile,
            'email' => $email,
            'password' => md5($password),
            'profile_pic' => $profilepic,
            'gender' => $gender,
            'web_url' => $weburl,
            'company_name' => $companyname,
            'profession' => $profession,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'pin_code' => $pincode,
            'status' => $status,
            'created_date' => $created_date,
            'updated_date' => $updated_date,
        );

        $user_id = $this->UM->insert('tbl_user_details', $userdataarr);
        if($user_id > 0){
            $this->session->set_flashdata('success_msg', lang('MSG_USER_ADD'));
        }else{
            $this->session->set_flashdata('error_msg', lang('MSG_USER_FAIL'));
        }

        redirect(BASE_URL . 'User/listUser');
    }

    public function listUser()
    {
        $this->load->view('listofusers.php');
    }

    public function listOFUser()
    {
        $sortColumn = ['name', 'email', 'mobile', 'status', 'gender'];

        // print_r($sortColumn); exit();
        $draw = intval($this->input->post("draw"));
        $offset = intval($this->input->post("start"));
        $limit = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $order = $this->input->post("order");

        $searchText = "";
        if (!empty($search['value'])) {
            $searchText = $search['value'];
        }

        $column = $order[0]['column'];
        $sort_by = $order[0]['dir'];
        $order_by = $sortColumn[$column];
        $whr = array('status !=' =>'deleted');
        $allrecord = $this->UM->getBywhere('tbl_user_details' , $whr);
        $listofUser = $this->UM->userData($searchText, $order_by, $sort_by, $offset, $limit);
        $data = array();
        $count = $offset;
        foreach ($listofUser as $user) {
            $count++;
            $data[] = array(
                $count,
                $user['name'],
                $user['email'],
                $user['mobile'],
                $user['status'],
                '<button class="btn btn-primary paddingButton" id="view" title="View" target="'. BASE_URL.'User/viewUser" data-toggle="modal"  data-div-target="view-modal" data-id="'.$user['user_id'].'"><i class="fa fa-eye" aria-hidden="true"></i></button> 
                <a href="'.BASE_URL.'User/getUserForEdit/'.$user['user_id'].'"><button class="btn btn-warning paddingButton" id="edit" title="Edit"><i class="fa fa-pencil-alt" aria-hidden="true"></i></button></a> 
                  <button class="btn btn-danger paddingButton" id="delete" title="delete" data-id="'.$user['user_id'].'"><i class="fa fa-trash" aria-hidden="true"></i></button> ',
            );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($allrecord),
            "recordsFiltered" => count($allrecord),
            "data" => $data,
        );

        echo json_encode($output);
        exit();
    }

    public function viewUser(){
        $user_id = $this->input->post('id');
        $whr = array('user_id' => $user_id);
        $userData = $this->UM->getByWhere('tbl_user_details',$whr);
        $data['userdetail'] = $userData[0];
        $this->load->view('userview.php',$data);
    }

    public function deleteUSer(){
        $user_id = $this->input->post('id');
        $whr = array('user_id' => $user_id);
        $dataArr= array('status' => 'deleted');
        $upate = $this->UM->update('tbl_user_details' , $dataArr,$whr);
    }

    public function getUserForEdit(){
        $user_id = $this->uri->segment(4);
        $whr = array('user_id' => $user_id);
        $userData = $this->UM->getByWhere('tbl_user_details',$whr);
        $data['userdetail'] = $userData[0];
        $this->load->view('usereditview.php',$data);

    }

    public function editUser(){

        $user_id = $this->input->post('user_id');
        $name = $this->input->post('name');
        $username = $this->input->post('username');
        $mobile = $this->input->post('mobile');
        $email = $this->input->post('email');
        $gender = $this->input->post('gender');
        $weburl = $this->input->post('web_url');
        $companyname = $this->input->post('company_name');
        $profession = $this->input->post('profession');
        $address = $this->input->post('address');
        $city = $this->input->post('city');
        $state = $this->input->post('state');
        $country = $this->input->post('country');
        $pincode = $this->input->post('pin_code');
        $status = $this->input->post('status');
        $updated_date = date('Y-m-d H:i:s');

        $whr = array('user_id' => $user_id);

        $userdataarr = array(
            'name' => $name,
            'user_name' => $username,
            'mobile' => $mobile,
            'email' => $email,
            'gender' => $gender,
            'web_url' => $weburl,
            'company_name' => $companyname,
            'profession' => $profession,
            'address' => $address,
            'city' => $city,
            'state' => $state,
            'country' => $country,
            'pin_code' => $pincode,
            'status' => $status,
            'updated_date' => $updated_date,
        );
        if(!empty($_FILES['profile_pic']['name'])){
            $config['upload_path'] = CON_PROFILES_PATH;
            $config['allowed_types'] = CON_ALLOWED_IMAGE_TYPE;
            $ext = pathinfo($_FILES['profile_pic']['name'], PATHINFO_EXTENSION);
            $filename = time() . "." . $ext;
            $config['file_name'] = $filename;
            $config['overwrite'] = true;
            $this->load->library('upload', $config);
    
            if (!$this->upload->do_upload('profile_pic')) {
                $this->session->set_flashdata('error_msg', lang('MSG_FILE_UPLOAD_FAIL'));
            redirect(BASE_URL . 'User/getUserForEdit/'.$user_id);
            } else {

                $fileData = $this->upload->data();
                $main_image = $fileData['file_name'];
                $userdataarr['profile_pic']= $main_image;
            }

        }
        $update = $this->UM->update('tbl_user_details' , $userdataarr,$whr);
        if($update == 1){
            $this->session->set_flashdata('success_msg', lang('MSG_EDIT_SUCCESS'));
        }else{
            $this->session->set_flashdata('error_msg', lang('MSG_EDIT_FAIL'));

        }
        redirect(BASE_URL . 'User/listUser');
        
    }

}
