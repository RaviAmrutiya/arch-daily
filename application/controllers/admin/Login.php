<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Login extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        $this->setLanguage($this->input->post_get('language'));
        $this->load->model('login_Model', 'LoginM');
    }

    public function index(){
        if($this->input->post()){
            $email = $this->input->post('username');
            $pass = $this->input->post('password');
            $loginData = $this->LoginM->doLogin($email,$pass);
            if(!empty($loginData)){
                $this->auth->login($loginData);
                $this->session->set_flashdata('login_success', lang('MSG_LOGIN_SUCCESS'));
                redirect(BASE_URL . 'dashboard');
            }else{
                $this->session->set_flashdata('login_fail', lang('MSG_LOGIN_FAIL'));
                redirect(BASE_URL . 'login');
            }

        }else{

            $this->load->view('login');
        }
    }
  
    public function logout(){
        $this->auth->logout();
		redirect(BASE_URL);
	}
}

?>