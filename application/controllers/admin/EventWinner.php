<?php
defined('BASEPATH') or exit('No direct script access allowed');

class EventWinner extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        if (!$this->auth->isLogin()) {
            redirect(CON_LOGIN_PATH);
        }
        $this->setWebLanguage();

        $this->load->model('Event_Winner_Model','EWM');
    }

    public function index()
    {
        $this->load->view('listEventWinner.php');
    }

    public function addeventwinner()
    {
        if($this->input->post()){
            $user_id = $this->input->post('user_id');
            $event_id = $this->input->post('event_id');
            $rank = $this->input->post('rank');
            $price = $this->input->post('price');
            $created_date = date('Y-m-d H:i:s');
            $updated_date = date('Y-m-d H:i:s');
            $status='active';

            $winnerData = array(
                'user_id'=>$user_id,
                'event_id'=>$event_id,
                'winning_prize'=> $price,
                'status' => $status,
                'created_date' => $created_date,
                'updated_date' => $updated_date,
            );
            $insertWinner = $this->EWM->insert('event_winner',$winnerData);
            if($insertWinner > 0){
                $this->session->set_flashdata('success_msg', lang('WINNER_ADDED'));
                redirect(BASE_URL . 'EventWinner');
            } else {
                $this->session->set_flashdata('error_msg', lang('WINNER_FAIL'));
                redirect(BASE_URL . 'EventWinner/addeventwinner');
            }

            
        }
        $whr = array('status'=>'active');
        $where = array('status' => 'open');
        $getuserdata = $this->EWM->getByWhere('tbl_user_details',$whr);
        $geteventdata = $this->EWM->getByWhere('event',$where);
        $data['eventdetails']= $geteventdata;
        $data['userdetails']=$getuserdata;
        //print_r($data1);exit();
        $this->load->view('addEventWinner.php',$data,$data);
        
    }
    public function listEventWinner()
    {

        $sortColumn = ['winner_id','event_name','name','winning_prize','rank'];

        //print_r($sortColumn); exit();
        $draw = intval($this->input->post("draw"));
        $offset = intval($this->input->post("start"));
        $limit = intval($this->input->post("length"));
        $search = $this->input->post("search");
        $order = $this->input->post("order");


        $searchText = "";
        if (!empty($search['value'])) {
            $searchText = $search['value'];
        }


        $column = $order[0]['column'];
        $sort_by = $order[0]['dir'];
        $order_by = $sortColumn[$column];

        $allrecord = $this->EWM->getAll('event_winner');
        $count = $offset;
        $listofevent = $this->EWM->listwinnerdata($searchText, $order_by, $sort_by, $offset, $limit);
        $data = array();
        foreach ($listofevent  as $eventwinner) {
            $count++ ;
            $data[] = array(
                $count,
                $eventwinner['name'],
                $eventwinner['event_name'],
                //date('Y-m-d',strtotime($event['start_date'])),
                //date('Y-m-d',strtotime($event['end_date'])),
                $eventwinner['winning_prize'],
                $eventwinner['rank'],
                 ' <button class="btn btn-danger paddingButton" id="delete" title="delete" data-id="'.$eventwinner['winner_id'].'"><i class="fa fa-trash" aria-hidden="true"></i></button> ',
                );
        }

        $output = array(
            "draw" => $draw,
            "recordsTotal" => count($allrecord),
            "recordsFiltered" => count($allrecord),
            "data" => $data
        );


        echo json_encode($output);
        exit();

    }

    public function deleteEventWinner(){
        $winner_id = $this->input->post('id');
        $whr = array('winner_id' => $winner_id );
        $dataArr= array('status' => 'deleted');
        $upate = $this->EWM->update('event_winner', $dataArr,$whr);
         
    }

    // public function viewEvent(){
    //     $winner_id = $this->input->post('id');
    //     $whr = array('winner_id' => $winner_id);
    //     $eventwinnerdata = $this->EM->getByWhere('event_winner',$whr);
    //     $data['eventwinnerdetail'] = $eventwinnerdata[0];
    //     $this->load->view('eventview.php',$data);
    // }
}
?>