<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard extends MY_Controller
{
    public function __construct()
    {
        parent::__construct();
        // if (empty($this->input->post('language'))) {
        //     $this->resArr[CON_RES_MESSAGE] = 'Unauthorized Access';
        //     $this->sendResponse($this->resArr);
        // }
        // require_once APPPATH . "/third_party/apns/PushNotification.php";
        if (!$this->auth->isLogin()) {
            redirect(CON_LOGIN_PATH);
        }
        $this->setWebLanguage();

        $this->load->model('Dashboard_Model','DM');
    }
    
    public function index(){
        $total['user'] = $this->DM->getTotalUSer();
        $total['event']=$this->DM->getTotalEvent();
        $total['event_winner'] = $this->DM->getTotalWinners();
        $total['votes'] =$this->DM->getTotalVotes();
        $total['open_events'] = $this->DM->getDiffEvents('open');
        $total['close_events'] = $this->DM->getDiffEvents('close');
        $total['pre_events'] = $this->DM->getDiffEvents('preparing');

        $this->load->view('dashboard.php',$total);
    }
}
?>