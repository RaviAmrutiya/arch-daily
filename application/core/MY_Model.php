<?php
defined('BASEPATH') or exit('No direct script access allowed');

class MY_Model extends CI_Model
{

    public $return_type = 'array';

    public function __construct()
    {
        parent::__construct();
    }

    /* -------------- SELECT DATA -------------------------  */
    /*
     * Select All Rows
     */
    public function getAll($table_name)
    {
        try {
            $query = $this->db->get($table_name);
            return $this->returnRows($query);
        } catch (Exception $ex) {
            return false;
        }
    }

    /*
     * Select rows by where clause
     */
    public function getByWhere($table_name, $colValArr = array())
    {
        try {
            $query = $this->db->get_where($table_name, $colValArr);
            return $this->returnRows($query);
        } catch (Exception $ex) {
            return false;
        }
    }

    /*
     *  Change Return as Object or Array
     */
    protected function returnRows($query)
    {
        if ($query == null) {
            return array();
        }
        if ($this->return_type == 'object') {
            return $query->result();
        } else if ($this->return_type == 'array') {
            return $query->result_array();
        }
    }

    /* -------------- INSERT DATA -------------------------  */

    public function insert($table_name, $data = array())
    {
        try
        {
            if ($this->db->insert($table_name, $data)) {
                return $this->db->insert_id();
            } else {
                return 0;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /* -------------- UPDATE DATA -------------------------  */

    public function update($table_name, $data = array(), $whereArr = array())
    {
        try {
            $this->db->where($whereArr);
            if ($this->db->update($table_name, $data)) {
                return $this->db->affected_rows();
            } else {
                return 0;
            }
        } catch (Exception $ex) {
            return false;
        }
    }

    /* -------------- DELETE DATA -------------------------  */
    public function delete($table_name, $whereArr = array())
    {
        try {
            if (is_array($whereArr)) {
                $this->db->where($whereArr);
                $this->db->delete($table_name);
                return $this->db->affected_rows();
            } else {
                return 0;
            }
        } catch (Exception $ex) {
            return false;
        }
    }
}
