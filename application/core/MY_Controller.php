<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class MY_Controller extends CI_Controller {

    public function __construct() {
        parent::__construct();		
    }

    public function sendResponse($res_param = array(),$type = 'json')
	{
		if($type === 'json')
		{
			//$this->output->set_content_type('application/json', 'utf-8');
			header('Content-Type: application/json');
			echo json_encode($res_param);
			exit();
		}
    }
    
    public function setWebLanguage()
	{
		$site_lang = CON_DEFAULT_SITE_LANGUAGE;
		if($this->session->userdata('site_language'))
		{
			$site_lang = $this->session->userdata('site_language');
		}
		else
		{
			$this->session->set_userdata('site_language',CON_DEFAULT_SITE_LANGUAGE);
		}
		$this->load->language ( 'message', $site_lang );
		$this->config->set_item('language', $site_lang);
    }
    
    public function setLanguage($lang = '')
	{
		$site_lang = CON_DEFAULT_SITE_LANGUAGE;
		if(!empty($lang))
		{
			$site_lang = $lang;
		}
		$this->load->language ( 'message', $site_lang );
		$this->config->set_item('language', $site_lang);
    }
}
