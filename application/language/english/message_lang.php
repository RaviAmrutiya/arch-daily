<?php

// SITE LEVEL
$lang['site_name_long'] = 'Arch Daily';
$lang['site_name_short'] = 'AD';

//API
$lang['MSG_INVALID_PARAMETER'] = "Parameter Missing or Invalid Parameters.";
$lang['MSG_LOGIN_FAIL'] = "Credential invalid, check your login credentials";
$lang['MSG_LOGIN_SUCCESS'] = "Login success";
$lang['MSG_SIGNUP_FAIL'] = "Signup error occured, please try later";
$lang['MSG_SIGNUP_SUCCESS'] = "Signup success";
$lang['MSG_EMAIL_EXISTS'] = "Email already exists";
$lang['MSG_USERNAME_EXISTS'] = "User Name already exists";
$lang['MSG_LIST_EVENT'] = "List Of Event";
$lang['MSG_LIST_EVENT_EMPTY'] = "List Of Event Empty";
$lang['MSG_EVENT_DETAIL']="Event detail";
$lang['MSG_EVENT_EMPTY']="Event detail Empty";
$lang['MSG_ADD_MAPPING']="User Joined Event";
$lang['MSG_ADD_MAPPING_FAIL']="Joining Event Failed";
$lang['MSG_EDIT_SUCCESS']="User Edited Successfully";
$lang['MSG_EDIT_FAIL']="Some Issue In User Editing";
$lang['MSG_FILE_UPLOAD_FAIL'] = "Some Issue In File Uploading";
$lang['open'] = "Open";
$lang['close'] = "Close";
$lang['preparing'] = "Preparing";
$lang['vote_done'] = "Voteing Success";
$lang['vote_fail'] = "Voteing Fail";
$lang['already_voted']="Already Voted";
$lang['MSG_CANT_JOIN']="You Can't Join";
$lang['MSG_LIST_SUBFILES'] = "List Of SUbfiles";
$lang['MSG_LIST_SUBFILES_EMPTY'] = "List Of subfiles Empty";
$lang['MSG_LIST_WINNER'] = "List Of Winner";
$lang['MSG_LIST_WINNER_EMPTY'] = "List Of winner Empty";
$lang['MSG_MOBILE_EXISTS'] = "Phone no already exists";
$lang['EVENT_ADDED']="Event Added";
$lang['EVENT_FAIL']="Could Not Add Event";
$lang['MSG_USER_EXISTS'] = "User already joined";
$lang['MSG_USER_ADD'] = "User Added Successfully";
$lang['MSG_USER_FAIL'] = "Cannot Add User";
$lang['VALID_EMAIL']="valid email";
$lang['VALID_MOBILE']="valid mobile";
$lang['VALID_USER']="valid username";
$lang['OTP_DONE']="Otp Generated";
$lang['OTP_FAIL']="Otp Generating Fail";
$lang['EMAIL_FAIL']="Email Not sent";
$lang['OPT_MISMATCH']="Otp does not match";
$lang['USER_NOT_FOUND']="User Not Found";
$lang['WRONG_OLD_PASS']="Wrong Old Password";
$lang['PASSWORD_CHANGE']="Password Changed";
$lang['OTP_VERIFIED']="Otp Verified";
$lang['PASSWORD_CHANGE_FAIL']="Issue in changing password";


// ADMIN PANNEL 

// FOR DASHBOARD 
$lang['DASHBOARD'] = "Dashbord";
$lang['TOTAL_USERS'] = "Total Users";
$lang['TOTAL_EVENTS'] = "Total Events";
$lang['TOTAL_EVENT_WINNER']="Total Event Winners";
$lang['TOTAL_VOTES'] = "Total Votes";
$lang['MORE_INFO']="More info ";
$lang['OPEN_EVENTS']="Open Events";
$lang['PREP_EVENTS']="Preparing Events";
$lang['CLOSE_EVENTS']="Close Events";

// FOR USER 
$lang['USER']="User";
$lang['NAME']="Name";
$lang['USER_NAME']="User Name";
$lang['MOBILE']="Mobile";
$lang['EMAIL'] = "Email";
$lang['STATUS'] = "Status";
$lang['PASSWORD']="Password";
$lang['PROFILE_PIC']="Profile Pic";
$lang['ACTIONS'] = 'Action';
$lang['GENDER'] = 'Gender';
$lang['WEB_URL'] = 'Web Url';
$lang['COMPANY_NAME'] = 'Company Name';
$lang['PROFESSION'] = 'Profession';
$lang['ADDRESS'] = 'Address';
$lang['CITY'] = 'City';
$lang['STATE'] = 'State';
$lang['COUNTRY'] = 'Country';
$lang['PIN_CODE'] = 'Pin-code';
$lang['CREATED_DATE']="Joined Date";
$lang['USER_DETAIL']="User Details";
$lang['CLOSE']="Close";
$lang['SR_NO'] = "Index";
$lang['USER_LIST'] = "User List";
$lang['CARD_TITLE'] = "List Of Users With Full Feature";
$lang['ADD_USER']="Add User";


// FOR EVENT 
$lang['Event_Name'] = 'Event Name';
$lang['Event_Description'] = 'Description';
$lang['Event_Start_Date'] = 'Start Date';
$lang['Event_End_Date'] = 'End Date';
$lang['Event_Fees'] = 'Fees';
$lang['Event_Max_Contestant'] = 'Max Contestant';
$lang['Winning_Prize'] = 'Winning Prize';
$lang['Event_Status'] = 'Status';
$lang['Event_Created_Date'] = 'Created Date';

// FOR EVENT WINNER
$lang['WINNER_ADDED']="Winner Added";
$lang['WINNER_FAIL']="Winner Fail";

// FOR EVENT PARTICIPATION
$lang['JOIN_USER']="Join User";
$lang['EVENT_PARTICIPATION']="Event Participation";

// FOR EVENT WINNER
$lang['USER_NAME']='user name';
$lang['EVENT_NAME']='event name';
$lang['PRIZE']='prize';
$lang['RANK']='rank';