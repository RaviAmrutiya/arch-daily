<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Dashboard_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function getTotalUSer(){
        $sql = "SELECT COUNT(user_id) AS total_user FROM tbl_user_details WHERE status='active'";
        $result= $this->db->query($sql);
        return $this->returnRows($result);
    }

    public function getTotalEvent(){
        $sql = "SELECT COUNT(event_id) AS total_event FROM event";
        $result= $this->db->query($sql);
        return $this->returnRows($result);
    }

    public function getTotalWinners(){
        $sql = "SELECT COUNT(winner_id) AS winners FROM event_winner WHERE status='active' ";
        $result= $this->db->query($sql);
        return $this->returnRows($result);
    }

    public function getTotalVotes(){
        $sql = "SELECT COUNT(voting_id) AS votes FROM tbl_voteing WHERE status='active'";
        $result= $this->db->query($sql);
        return $this->returnRows($result);
    }

    public function getDiffEvents($status){
    $sql = "SELECT COUNT(event_id) AS Events FROM event WHERE status='{$status}'";
        $result= $this->db->query($sql);
        return $this->returnRows($result);
    }
    
}