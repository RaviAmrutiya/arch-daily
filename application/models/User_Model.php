<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function userData($searchText, $order_by, $sort_by, $offset, $limit)
    {
        $offset_limit = "";
        if (!empty($limit)) {
            $offset_limit = "LIMIT {$offset},{$limit}";
        }
        $order_by = "ORDER BY {$order_by} {$sort_by}";
        $like = "";

        if(!empty($searchText)){
            $like = "AND (name LIKE  '%{$searchText}%' OR email LIKE  '%{$searchText}%' OR mobile LIKE  '%{$searchText}%' OR gender LIKE  '%{$searchText}%')";
        }
        $sql = "SELECT user_id,name,email,mobile,status,gender FROM tbl_user_details WHERE status != 'deleted' {$like} {$order_by} {$offset_limit}";
        // print_r($sql);
        $result = $this->db->query($sql);

        return $this->returnRows($result);
    }
}
