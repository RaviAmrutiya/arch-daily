<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event_Winner_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listwinnerdata($searchText, $order_by, $sort_by, $offset, $limit)
    {

        $offset_limit = "";
        if (!empty($limit)) {
            $offset_limit = "LIMIT {$offset},{$limit}";
        }
        $order_by = "ORDER BY {$order_by} {$sort_by}";
        $like = "";
        if (!empty($searchText)) {
            $like = " WHERE (name LIKE '%{$searchText}%' OR event_name LIKE '%{$searchText}%' OR t3.winning_prize LIKE '%{$searchText}%') ";
        }

        $sql = "SELECT t3.winner_id,t2.event_name,t1.name,t3.winning_prize,t3.rank FROM event_winner t3 INNER JOIN tbl_user_details t1 ON t3.user_id=t1.user_id INNER JOIN event t2 ON t3.event_id=t2.event_id  {$like} {$order_by} {$offset_limit}";
        $result = $this->db->query($sql);

        return $this->returnRows($result);

    }

   
}
?>