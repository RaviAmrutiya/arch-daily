<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Event_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    public function listeventdata($searchText, $order_by, $sort_by, $offset, $limit)
    {

        $offset_limit = "";
        if (!empty($limit)) {
            $offset_limit = "LIMIT {$offset},{$limit}";
        }
        $order_by = "ORDER BY {$order_by} {$sort_by}";
        $like = "";
        if (!empty($searchText)) {
            $like = " WHERE (event_name LIKE '%{$searchText}%' OR description LIKE '%{$searchText}%' OR status LIKE '%{$searchText}%' ";
        }

        $sql = "SELECT event_id,event_name,description,start_date,end_date,fees,max_contestant,winning_prize,status FROM event {$like} {$order_by} {$offset_limit}";
        $result = $this->db->query($sql);

        return $this->returnRows($result);

    }

}
