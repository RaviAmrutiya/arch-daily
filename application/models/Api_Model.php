<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Api_Model extends MY_Model
{

    public function __construct()
    {
        parent::__construct();
    }

    /* Login */
    public function checkLogin($emailOrPhone, $password)
    {
        $pass = md5($password);
        $qry = "SELECT user_id,name,user_name,mobile,email,profile_pic,CONCAT('" . CON_PROFILES_URL . "',profile_pic) AS profile_pic_path,gender,web_url,company_name,profession,address,city,state,country,pin_code,status,created_date,updated_date FROM tbl_user_details WHERE  (email='{$emailOrPhone}' OR mobile='{$emailOrPhone}') AND   password='{$pass}' AND  status='active' ";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function listEvent(){
        $qry = "SELECT event_id,event_name,main_banner,CONCAT('" . CON_BANNER_URL . "',main_banner) AS main_banner_path,description,start_date,end_date,fees,max_contestant,winning_prize,status,created_date,updated_Date FROM event ORDER BY status='close',status='open',status='preparing', created_date DESC";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function detailEvent($event_id){
    $qry = "SELECT event_id,event_name,main_banner,CONCAT('" . CON_BANNER_URL . "',main_banner) AS main_banner_path,description,start_date,end_date,fees,max_contestant,winning_prize,status,created_date,updated_Date FROM event WHERE event_id={$event_id}";
    $result = $this->db->query($qry);
    return $this->returnRows($result);
    }

    public function getUserOfEvent($event_id){
        $qry = "SELECT t2.user_id,t2.name,t2.user_name,t2.mobile,t2.email,t2.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t2.profile_pic_path) AS profile_pic_path,t2.gender,t2.web_url,t2.company_name,t2.profession,t2.address,t2.city,t2.state,t2.country,t2.pin_code,t2.status,t2.created_date,t2.updated_date 
        FROM user_mapping t1 INNER JOIN tbl_user_details t2 ON t1.user_id=t2.user_id WHERE t1.event_id={$event_id} ORDER BY t2.created_date DESC";
           $result = $this->db->query($qry);
           return $this->returnRows($result);

    }

    public function updatedUser($user_id){
        $qry = "SELECT user_id,name,user_name,mobile,email,profile_pic,CONCAT('" . CON_PROFILES_URL . "',profile_pic) AS profile_pic_path,gender,web_url,company_name,profession,address,city,state,country,pin_code,status,created_date,updated_date FROM tbl_user_details WHERE  user_id={$user_id} ";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getMainFiles($user_id){
        $qry="SELECT event_file_id,main_file, CONCAT('" . CON_MAIN_FILES_URL . "',main_file) AS main_file_path FROM event_files WHERE user_id={$user_id} ORDER BY created_date DESC";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getSubFiles($user_id){
        $qry="SELECT event_file_id,sub_file, CONCAT('" . CON_SUB_FILES_URL . "',sub_file) AS sub_file_path FROM event_files WHERE user_id={$user_id} ORDER BY created_date DESC";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getVoteCount($event_id,$user_id){
        $qry = "SELECT COUNT(*) AS total_vote FROM tbl_voteing WHERE to_vote_id={$user_id} AND event_id={$event_id} AND status='active' AND voted='yes'";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function Userdetail(){
        $qry = "SELECT * FROM tbl_user_details";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }


    public function getUserOfEventList($event_id){
        $qry = "SELECT t2.user_id,t2.name,t2.user_name,t2.mobile,t2.email,t2.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t2.profile_pic_path) AS profile_pic_path,t2.gender,t2.web_url,t2.company_name,t2.profession,t2.address,t2.city,t2.state,t2.country,t2.pin_code,t2.status,t2.created_date,t2.updated_date 
        FROM user_mapping t1 INNER JOIN tbl_user_details t2 ON t1.user_id=t2.user_id WHERE t1.event_id={$event_id} ORDER BY t2.created_date DESC";
           $result = $this->db->query($qry);
           return $this->returnRows($result);

    }

    public function getUserdetailOfEventList($event_id,$user_id){
        $qry = "SELECT t2.user_id,t2.name,t2.user_name,t2.mobile,t2.email,t2.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t2.profile_pic_path) AS profile_pic_path,t2.gender,t2.web_url,t2.company_name,t2.profession,t2.address,t2.city,t2.state,t2.country,t2.pin_code,t2.status,t2.created_date,t2.updated_date 
        FROM user_mapping t1 INNER JOIN tbl_user_details t2 ON t1.user_id=t2.user_id WHERE t1.event_id={$event_id} AND t1.user_id={$user_id} ORDER BY t2.created_date DESC";
           $result = $this->db->query($qry);
           return $this->returnRows($result);

    }

    public function getfiledetail($event_id,$user_id){
        $qry = "SELECT CONCAT('" . CON_SUB_FILES_URL . "',sub_file) AS sub_file_path from event_files where event_id={$event_id} AND user_id={$user_id} AND status='active' ";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getvoterdetail($event_id,$user_id){
        $qry = "SELECT t2.* FROM tbl_voteing t1 INNER JOIN tbl_user_details t2 ON t1.voter_id=t2.user_id WHERE t1.event_id={$event_id} AND t1.to_vote_id={$user_id} ORDER BY t1.created_date DESC";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getdate($event_id,$date){
    $qry = "SELECT * FROM event WHERE start_date > '{$date}' AND event_id={$event_id}";
    $result = $this->db->query($qry);
    return $this->returnRows($result);
    }

    public function detailsubfiles(){
        $qry = "SELECT t2.user_id,t2.name,t2.user_name,t2.mobile,t2.email,t2.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t2.profile_pic_path) AS profile_pic_path,t2.gender,t2.web_url,t2.company_name,t2.profession,t2.address,t2.city,t2.state,t2.country,t2.pin_code,t2.status,t2.created_date,t2.updated_date,CONCAT('" . CON_SUB_FILES_URL . "',t1.sub_file) AS sub_file_path FROM event_files t1 INNER JOIN tbl_user_details t2 ON t1.user_id = t2.user_id ORDER BY t1.created_date DESC ";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function allwinnerlist(){
        $qry= "SELECT t4.event_name,t2.user_id,t2.event_id,t2.winning_prize,t1.user_id,t1.name,t1.user_name,t1.mobile,t1.email,t1.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t1.profile_pic_path) AS profile_pic_path,t1.gender,t1.web_url,t1.company_name,t1.profession,t1.address,t1.city,t1.state,t1.country,t1.pin_code,t1.status,t1.created_date,t1.updated_date FROM event_winner t2 
        INNER JOIN tbl_user_details t1 ON t1.user_id=t2.user_id INNER JOIN event t4 ON t4.event_id=t2.event_id";
        // $qry = "SELECT t1.event_id FROM event_winner t1";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function userwinnerlist($user_id){
        $qry= "SELECT t2.user_id,t2.event_id,t2.winning_prize,t1.user_id,t1.name,t1.user_name,t1.mobile,t1.email,t1.profile_pic,CONCAT('" . CON_PROFILES_URL . "',t1.profile_pic_path) AS profile_pic_path,t1.gender,t1.web_url,t1.company_name,t1.profession,t1.address,t1.city,t1.state,t1.country,t1.pin_code,t1.status,t1.created_date,t1.updated_date FROM event_winner t2 INNER JOIN tbl_user_details t1 ON t1.user_id=t2.user_id  WHERE t2.user_id ={$user_id}";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function gettotalvote($user_id){
        $qry = "SELECT COUNT(vote) as session_user_total_vote FROM tbl_voteing WHERE to_vote_id={$user_id}";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function gettotalparticipate($user_id){
        $qry="SELECT COUNT(event_id) AS total_participate FROM user_mapping WHERE user_id='{$user_id}'";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }

    public function getDataForotp($user_id,$otpFor){
   $qry="SELECT * FROM tbl_otp WHERE user_id='{$user_id}' AND otp_for='{$otpFor}' AND status='unverified' ORDER BY created_date DESC LIMIT 1";
        $result = $this->db->query($qry);
        return $this->returnRows($result);
    }
}
